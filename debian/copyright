Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Test-Skip-UnlessExistsExecutable
Upstream-Contact: Takatoshi Kitano <techmemo@gmail.com>
Source: https://metacpan.org/release/Test-Skip-UnlessExistsExecutable

Files: *
Copyright: 2010, Takatoshi Kitano <techmemo@gmail.com>
License: Artistic or GPL-1+

Files: inc/Module/*
Copyright: 2002-2012, Adam Kennedy <adamk@cpan.org>
 2002-2012, Audrey Tang <autrijus@autrijus.org>
 2002-2012, Brian Ingerson <ingy@cpan.org>
License: Artistic or GPL-1+

Files: inc/Spiffy.pm
Copyright: 2004-2014, Ingy döt Net <ingy@cpan.net>
License: Artistic or GPL-1+

Files: inc/Test/Base/* inc/Test/Base.pm
Copyright: 2005-2018, Ingy döt Net <ingy@cpan.org>
License: Artistic or GPL-1+

Files: inc/Test/Builder/* inc/Test/Builder.pm
Copyright: 2020, Chad Granum <exodist@cpan.org>
License: Artistic or GPL-1+

Files: inc/Test/More.pm
Copyright: 2001-2008 by Michael G Schwern <schwern@pobox.com>
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2022-2023, Mason James <mtj@kohaaloha.com>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.
